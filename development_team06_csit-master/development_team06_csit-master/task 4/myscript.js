count = 1;
start = false;

function StartProgram() {
    start = true;
    count = 1;
    document.getElementById("line1").style.color = "";
    document.getElementById("line2").style.color = "";
    document.getElementById("line3").style.color = "";
    document.getElementById("line4").style.color = "";
}

function progs() {
    if (start) {
        if (count == 1) {
            document.getElementById("myOutput").innerHTML = " ";
            document.getElementById("myOutput").innerHTML = "This is Line 1";
            document.getElementById("line1").style.color = "#b309b3";
            document.getElementById("line2").style.color = "";
            document.getElementById("line3").style.color = "";
            document.getElementById("line4").style.color = "";
            // console.log(1);
            count++;
            return;
        }
        if (count == 2) {
            document.getElementById("myOutput").innerHTML = " ";
            document.getElementById("myOutput").innerHTML = "This is Line 2";
            document.getElementById("line2").style.color = "#b309b3";
            document.getElementById("line1").style.color = "";
            document.getElementById("line3").style.color = "";
            document.getElementById("line4").style.color = "";
            // console.log(1);
            count++;
            return;
        }
        if (count == 3) {
            document.getElementById("myOutput").innerHTML = " ";
            document.getElementById("myOutput").innerHTML = "This is Line 3";
            document.getElementById("line3").style.color = "#b309b3";
            document.getElementById("line1").style.color = "";
            document.getElementById("line2").style.color = "";
            document.getElementById("line4").style.color = "";
            // console.log(1);
            count++;
            return;
        }
        if (count == 4) {
            document.getElementById("myOutput").innerHTML = " ";
            document.getElementById("myOutput").innerHTML = "This is Line 4";
            document.getElementById("line4").style.color = "#b309b3";
            document.getElementById("line1").style.color = "";
            document.getElementById("line2").style.color = "";
            document.getElementById("line3").style.color = "";
            // console.log(1);
            count = 1;
            return;
        }
    }
}
//var g = document.getElementById("textbox2").innerHTML = "Line 1";
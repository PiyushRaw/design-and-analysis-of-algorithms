count = 0;
start = false;

document.getElementById("next-page").disabled = true;
document.getElementById("next-page").style.color = "black";
document.getElementById("next-page").style.backgroundColor = "grey";
document.getElementById("Back").disabled = true;
document.getElementById("Back").style.color = "black";
document.getElementById("Back").style.backgroundColor = "grey";

function Startprograms() {
    document.getElementById("myBtn").disabled = true;
    document.getElementById("myBtn").style.color = "black";
    document.getElementById("myBtn").style.backgroundColor = "grey";
    const ArrayofLine = document.getElementsByClassName('line');
    let inde = -1;

    Next.addEventListener('click', function() {
        inde++;
        ArrayofLine[inde].classList.add('line-highlight');
        if (inde > 0) ArrayofLine[inde - 1].classList.remove('line-highlight');
        if (inde == 0) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = " Declaring Libraries of C++";
        }
        if (inde == 1) {
            document.getElementById("Back").disabled = false;
            document.getElementById("Back").style.color = "";
            document.getElementById("Back").style.backgroundColor = "";
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = " Using namespace named std(Standard)";
        }
        if (inde == 2) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = " Here we make structure for an item which stores weight and corresponding item value.";
        }
        if (inde == 3) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = " Here is opening braces";
        }
        if (inde == 4) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = " Here we declare the variable which store alue and weight of item.";
        }
        if (inde == 5) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = " Here we make a default constructor which does nothing.";
        }
        if (inde == 6) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = " Here is constructor opening and closing braces.";
        }
        if (inde == 7) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = " Here we close our structure by closing braces.";
        }
        if (inde == 8) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = " Here we make a Comparison function to sort Item according to val/weight ratio";
        }
        if (inde == 9) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = " Here is opening braces.";
        }
        if (inde == 10) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = " Here we store the ratio of value and weight in r1.";
        }
        if (inde == 11) {
            /*let elem = document.getElementById("codeContentBP22");
            elem.scrollIntoView(true);*/
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = " Here we store the ratio of value and weight in r2.";
        }
        if (inde == 12) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = " Here we return the bigger between r1 and r2.";
        }
        if (inde == 13) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = " Here is closing braces.";
        }
        if (inde == 14) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = " Here we make our Main greedy function to solve problem.";
        }
        if (inde == 15) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = " Here is opening braces.";
        }
        if (inde == 16) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = " Here we call a function sorting Item on basis of ratio. ";
        }
        if (inde == 17) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = " Here we make a for loop which runs upto n.";
        }
        if (inde == 18) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = " Here is closing braces.";
        }
        if (inde == 19) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = " Here we print value and weight of item using output statement. ";
        }
        if (inde == 20) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = " Here is closing braces.  ";
        }
        if (inde == 21) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = " Here we declare a variable to store Current weight in knapsack.";
        }
        if (inde == 22) {
            /*let elem = document.getElementById("codeContentBP22");
            elem.scrollIntoView(true);*/
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = " Here we declare a varible to store Result (value in Knapsack). ";
        }
        if (inde == 23) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = " Here we make the for loop which runs upto n for Looping through all Items.";
        }
        if (inde == 24) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = " Here is opening braces";
        }
        if (inde == 25) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = " Here we make a if statetment because If adding Item won't overflow, add it completely";
        }
        if (inde == 26) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = " Here is opening braces.";
        }
        if (inde == 27) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = " Here we store the Current weight of knapsack.";
        }
        if (inde == 28) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = " Here we store the Final Value of Knapsack.";
        }
        if (inde == 30) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = " Here we else because If we can't add current Item, add fractional part of it.";
        }
        if (inde == 29) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = "Here is opening braces.";
        }
        if (inde == 31) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = "Here is opening braces.";
        }
        if (inde == 32) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = " Here we declare variable which store remaining weight.";
        }
        if (inde == 33) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = " Here we store the final value of knapsack.";
        }
        if (inde == 34) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = " Here we break our looping through using break statement.";
        }
        if (inde == 35) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = " Here is closing braces.";
        }
        if (inde == 36) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = " Here is closing braces.";
        }
        if (inde == 37) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = " Here we return our final value via return statement.";
        }
        if (inde == 38) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = " Here is closing braces.";
        }
        if (inde == 39) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = " Here we make our main function which is to test above function.";
        }
        if (inde == 40) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = " Here is opening braces.";
        }
        if (inde == 41) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = " Here we declare a variable which store the desire weight of knapsack.";
        }
        if (inde == 42) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = " Here we make an array which store the value and weight.";
        }
        if (inde == 43) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = " Here we declare a variable n which store the size of array.";
        }
        if (inde == 44) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = " Here we print the maximum value via output statement.";
        }
        if (inde == 45) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = " Here we make a return statement which returns zero.";
        }

        if (inde == 46) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = " closing main function";
            document.getElementById("next-page").disabled = false;
            document.getElementById("next-page").style.color = "";
            document.getElementById("next-page").style.backgroundColor = "";
            document.getElementById("Next").disabled = true;
            document.getElementById("Next").style.color = "black";
            document.getElementById("Next").style.backgroundColor = "grey";
            alert("The code has ended now move to console window");
        }
    });
    Back.addEventListener('click', function() {
        inde--;
        ArrayofLine[inde].classList.add('line-highlight');
        if (inde >= 0) ArrayofLine[inde + 1].classList.remove('line-highlight');
        if (inde == 0) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = " Declaring Libraries of C++";
            document.getElementById("Back").disabled = true;
            document.getElementById("Back").style.color = "black";
            document.getElementById("Back").style.backgroundColor = "grey";
        }
        if (inde == 1) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = " Using namespace named std(Standard)";
        }
        if (inde == 2) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = " Here we make structure for an item which stores weight and corresponding item value.";
        }
        if (inde == 3) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = " Here is opening braces";
        }
        if (inde == 4) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = " Here we declare the variable which store alue and weight of item.";
        }
        if (inde == 5) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = " Here we make a default constructor which does nothing.";
        }
        if (inde == 6) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = " Here is constructor opening and closing braces.";
        }
        if (inde == 7) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = " Here we close our structure by closing braces.";
        }
        if (inde == 8) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = " Here we make a Comparison function to sort Item according to val/weight ratio";
        }
        if (inde == 9) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = " Here is opening braces.";
        }
        if (inde == 10) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = " Here we store the ratio of value and weight in r1.";
        }
        if (inde == 11) {
            /*let elem = document.getElementById("codeContentBP22");
            elem.scrollIntoView(true);*/
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = " Here we store the ratio of value and weight in r2.";
        }
        if (inde == 12) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = " Here we return the bigger between r1 and r2.";
        }
        if (inde == 13) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = " Here is closing braces.";
        }
        if (inde == 14) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = " Here we make our Main greedy function to solve problem.";
        }
        if (inde == 15) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = " Here is opening braces.";
        }
        if (inde == 16) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = " Here we call a function sorting Item on basis of ratio. ";
        }
        if (inde == 17) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = " Here we make a for loop which runs upto n.";
        }
        if (inde == 18) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = " Here is closing braces.";
        }
        if (inde == 19) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = " Here we print value and weight of item using output statement. ";
        }
        if (inde == 20) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = " Here is closing braces.  ";
        }
        if (inde == 21) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = " Here we declare a variable to store Current weight in knapsack.";
        }
        if (inde == 22) {
            /*let elem = document.getElementById("codeContentBP22");
            elem.scrollIntoView(true);*/
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = " Here we declare a varible to store Result (value in Knapsack). ";
        }
        if (inde == 23) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = " Here we make the for loop which runs upto n for Looping through all Items.";
        }
        if (inde == 24) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = " Here is opening braces";
        }
        if (inde == 25) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = " Here we make a if statetment because If adding Item won't overflow, add it completely";
        }
        if (inde == 26) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = " Here is opening braces.";
        }
        if (inde == 27) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = " Here we store the Current weight of knapsack.";
        }
        if (inde == 28) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = " Here we store the Final Value of Knapsack.";
        }
        if (inde == 30) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = " Here we else because If we can't add current Item, add fractional part of it.";
        }
        if (inde == 29) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = "Here is opening braces.";
        }
        if (inde == 31) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = "Here is opening braces.";
        }
        if (inde == 32) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = " Here we declare variable which store remaining weight.";
        }
        if (inde == 33) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = " Here we store the final value of knapsack.";
        }
        if (inde == 34) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = " Here we break our looping through using break statement.";
        }
        if (inde == 35) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = " Here is closing braces.";
        }
        if (inde == 36) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = " Here is closing braces.";
        }
        if (inde == 37) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = " Here we return our final value via return statement.";
        }
        if (inde == 38) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = " Here is closing braces.";
        }
        if (inde == 39) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = " Here we make our main function which is to test above function.";
        }
        if (inde == 40) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = " Here is opening braces.";
        }
        if (inde == 41) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = " Here we declare a variable which store the desire weight of knapsack.";
        }
        if (inde == 42) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = " Here we make an array which store the value and weight.";
        }
        if (inde == 43) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = " Here we declare a variable n which store the size of array.";
        }
        if (inde == 44) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = " Here we print the maximum value via output statement.";
        }
        if (inde == 45) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = " Here we make a return statement which returns zero.";
            document.getElementById("Next").disabled = false;
            document.getElementById("Next").style.color = "";
            document.getElementById("Next").style.backgroundColor = "";
        }
        /*if (inde == 32) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = "returnfunction ran succesfully";
        }*/
        if (inde == 46) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = " closing main function";
            document.getElementById("Next").disabled = true;
            document.getElementById("Next").style.color = "black";
            document.getElementById("Next").style.backgroundColor = "grey";
        }
    });
}

function progs() {}

//var g = document.getElementById("textbox2").innerHTML = "Line 1"

function execute() {
    //for (i = 0; i < field1; i++) {
    count++;
    console.log(count);
    if (count <= 2) {
        var field1 = parseInt(document.getElementById("X").value);
        arr = Y.value.trim().split(' ');
        if (arr.length > field1)
            alert("Enter Correct values");
        else {
            arr.sort(function(a, b) { return a - b });
            document.getElementById("Output1").innerHTML = arr;
        }
        if (count == 2) {
            document.getElementById("myBtnexecute").disabled = true;
            document.getElementById("myBtnexecute").style.color = "black";
            document.getElementById("myBtnexecute").style.backgroundColor = "grey";
        }
    }
}
count = 0;
start = false;

document.getElementById("next-page").disabled = true;
document.getElementById("next-page").style.color = "black";
document.getElementById("next-page").style.backgroundColor = "grey";
document.getElementById("Back").disabled = true;
document.getElementById("Back").style.color = "black";
document.getElementById("Back").style.backgroundColor = "grey";

function Startprograms() {
    document.getElementById("myBtn").disabled = true;
    document.getElementById("myBtn").style.color = "black";
    document.getElementById("myBtn").style.backgroundColor = "grey";
    const ArrayofLine = document.getElementsByClassName('line');
    let inde = -1;

    Next.addEventListener('click', function() {
        inde++;
        ArrayofLine[inde].classList.add('line-highlight');
        if (inde > 0) ArrayofLine[inde - 1].classList.remove('line-highlight');
        if (inde == 0) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = " Declaring Libraries of C++";
        }
        if (inde == 1) {
            document.getElementById("Back").disabled = false;
            document.getElementById("Back").style.color = "";
            document.getElementById("Back").style.backgroundColor = "";
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = " Using namespace named std(Standard)";
        }
        if (inde == 2) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = "We define N to 4 then we will generalize our solution for n";
        }
        if (inde == 3) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = " printboard function opening";
        }
        if (inde == 4) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = " opening braces";
        }
        if (inde == 5) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = "printing matrix";
        }
        if (inde == 6) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = " printing matrix";
        }
        if (inde == 7) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = " printing matrix";
        }
        if (inde == 8) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = " printing matrix";
        }
        if (inde == 9) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = " closing braces";
        }
        if (inde == 10) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = " closing braces";
        }
        if (inde == 11) {
            /*let elem = document.getElementById("codeContentBP22");
            elem.scrollIntoView(true);*/
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = " isValid function for checking if position is valid we will check row and diagonals only as we have placed queen in column";
        }
        if (inde == 12) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = " checking row for position";
        }
        if (inde == 13) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = " if we find queen in that row return false";
        }
        if (inde == 14) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = " opening braces";
        }
        if (inde == 15) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = " returning false";
        }
        if (inde == 16) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = " closing braces";
        }
        if (inde == 17) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = " checking diagonals left side";
        }
        if (inde == 18) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = " if we find queen return false";
        }
        if (inde == 19) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = " return false";
        }
        if (inde == 20) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = "checking right side diagonal";
        }
        if (inde == 21) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = "if we find queen return false ";
        }
        if (inde == 22) {
            /*let elem = document.getElementById("codeContentBP22");
            elem.scrollIntoView(true);*/
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = " return false";
        }
        if (inde == 23) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = " closing braces";
        }
        if (inde == 24) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = " beginning of solveNQueen funtion";
        }
        if (inde == 25) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = " if we filled every column we return true";
        }
        if (inde == 26) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = " return true";
        }
        if (inde == 27) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = " iterate every column";
        }
        if (inde == 28) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = " opening braces";
        }
        if (inde == 29) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = " checking position of queen on board";
        }
        if (inde == 30) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = " opening braces";
        }
        if (inde == 31) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = " setting position as 1 meaning we have placed queen at that place";
        }
        if (inde == 32) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = " we recur for next columns";
        }
        if (inde == 33) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = " return true";
        }
        if (inde == 34) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = " if placing queen on this position does not lead to solution remove this from board";
        }
        if (inde == 35) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = " closing braces";
        }
        if (inde == 36) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = " return false";
        }
        if (inde == 37) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = " checksolution function";
        }
        if (inde == 38) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = " declaring matrix of size N*N";
        }
        if (inde == 39) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = " assiging matrix with 0 meaning empty spaces";
        }
        if (inde == 40) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = " assiging matrix with 0 meaning empty spaces";
        }
        if (inde == 41) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = " assiging matrix with 0 meaning empty spaces";
        }
        if (inde == 42) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = " if solveNQueen function fails meaning we are not able to place queens properly the return false";
        }
        if (inde == 43) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = " printing no solution exist";
        }
        if (inde == 44) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = " return false";
        }
        if (inde == 45) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = " closing brace";
        }
        /*if (inde == 32) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = "returnfunction ran succesfully";
        }*/
        if (inde == 46) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = " printboard function ";
        }
        if (inde == 47) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = " return true";
        }
        if (inde == 48) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = " closing brace ";
        }
        if (inde == 49) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = " main function";
        }
        if (inde == 50) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = " calling checkSolution function";
        }
        if (inde == 51) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = " return statement";
        }
        if (inde == 52) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = " closing main function";
            document.getElementById("Next").disabled = true;
            document.getElementById("Next").style.color = "black";
            document.getElementById("Next").style.backgroundColor = "grey";
            document.getElementById("next-page").disabled = false;
            document.getElementById("next-page").style.color = "";
            document.getElementById("next-page").style.backgroundColor = "";
            alert("The code has ended now move to console window");
        }
    });
    Back.addEventListener('click', function() {
        inde--;
        ArrayofLine[inde].classList.add('line-highlight');
        if (inde >= 0) ArrayofLine[inde + 1].classList.remove('line-highlight');
        if (inde == 0) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = " Declaring Libraries of C++";
            document.getElementById("Back").disabled = true;
            document.getElementById("Back").style.color = "black";
            document.getElementById("Back").style.backgroundColor = "grey";
        }
        if (inde == 1) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = " Using namespace named std(Standard)";
        }
        if (inde == 2) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = "We define N to 4 then we will generalize our solution for n";
        }
        if (inde == 3) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = " printboard function opening";
        }
        if (inde == 4) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = " opening braces";
        }
        if (inde == 5) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = "printing matrix";
        }
        if (inde == 6) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = " printing matrix";
        }
        if (inde == 7) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = " printing matrix";
        }
        if (inde == 8) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = " printing matrix";
        }
        if (inde == 9) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = " closing braces";
        }
        if (inde == 10) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = " closing braces";
        }
        if (inde == 11) {
            /*let elem = document.getElementById("codeContentBP22");
            elem.scrollIntoView(true);*/
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = " isValid function for checking if position is valid we will check row and diagonals only as we have placed queen in column";
        }
        if (inde == 12) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = " checking row for position";
        }
        if (inde == 13) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = " if we find queen in that row return false";
        }
        if (inde == 14) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = " opening braces";
        }
        if (inde == 15) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = " returning false";
        }
        if (inde == 16) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = " closing braces";
        }
        if (inde == 17) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = " checking diagonals left side";
        }
        if (inde == 18) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = " if we find queen return false";
        }
        if (inde == 19) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = " return false";
        }
        if (inde == 20) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = "checking right side diagonal";
        }
        if (inde == 21) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = "if we find queen return false ";
        }
        if (inde == 22) {
            /*let elem = document.getElementById("codeContentBP22");
            elem.scrollIntoView(true);*/
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = " return false";
        }
        if (inde == 23) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = " closing braces";
        }
        if (inde == 24) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = " beginning of solveNQueen funtion";
        }
        if (inde == 25) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = " if we filled every column we return true";
        }
        if (inde == 26) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = " return true";
        }
        if (inde == 27) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = " iterate every column";
        }
        if (inde == 28) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = " opening braces";
        }
        if (inde == 29) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = " checking position of queen on board";
        }
        if (inde == 30) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = " opening braces";
        }
        if (inde == 31) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = " setting position as 1 meaning we have placed queen at that place";
        }
        if (inde == 32) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = " we recur for next columns";
        }
        if (inde == 33) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = " return true";
        }
        if (inde == 34) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = " if placing queen on this position does not lead to solution remove this from board";
        }
        if (inde == 35) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = " closing braces";
        }
        if (inde == 36) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = " return false";
        }
        if (inde == 37) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = " checksolution function";
        }
        if (inde == 38) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = " declaring matrix of size N*N";
        }
        if (inde == 39) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = " assiging matrix with 0 meaning empty spaces";
        }
        if (inde == 40) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = " assiging matrix with 0 meaning empty spaces";
        }
        if (inde == 41) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = " assiging matrix with 0 meaning empty spaces";
        }
        if (inde == 42) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = " if solveNQueen function fails meaning we are not able to place queens properly the return false";
        }
        if (inde == 43) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = " printing no solution exist";
        }
        if (inde == 44) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = " return false";
        }
        if (inde == 45) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = " closing brace";
        }
        /*if (inde == 32) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = "returnfunction ran succesfully";
        }*/
        if (inde == 46) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = " printboard function ";
        }
        if (inde == 47) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = " return true";
        }
        if (inde == 48) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = " closing brace ";
        }
        if (inde == 49) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = " main function";
        }
        if (inde == 50) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = " calling checkSolution function";
        }
        if (inde == 51) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = " return statement";
        }
        if (inde == 52) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = " closing main function";
            // document.getElementById("myBtnexecute").disabled = false;
            // document.getElementById("myBtnexecute").style.color = "";
            // document.getElementById("myBtnexecute").style.backgroundColor = "";
            document.getElementById("Next").disabled = true;
            document.getElementById("Next").style.color = "black";
            document.getElementById("Next").style.backgroundColor = "grey";
        }
    });
}

function progs() {}

//var g = document.getElementById("textbox2").innerHTML = "Line 1"

function execute() {
    //for (i = 0; i < field1; i++) {
    count++;
    console.log(count);
    if (count <= 2) {
        var field1 = parseInt(document.getElementById("X").value);
        arr = Y.value.trim().split(' ');
        if (arr.length > field1)
            alert("Enter Correct values");
        else {
            arr.sort(function(a, b) { return a - b });
            document.getElementById("Output1").innerHTML = arr;
        }
        if (count == 2) {
            document.getElementById("myBtnexecute").disabled = true;
            document.getElementById("myBtnexecute").style.color = "black";
            document.getElementById("myBtnexecute").style.backgroundColor = "grey";
        }
    }
}
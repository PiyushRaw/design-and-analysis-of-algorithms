count = 0;
start = false;

document.getElementById("Back").disabled = true;
document.getElementById("Back").style.color = "black";
document.getElementById("Back").style.backgroundColor = "grey";
document.getElementById("next-page").disabled = true;
document.getElementById("next-page").style.color = "black";
document.getElementById("next-page").style.backgroundColor = "grey";

function Startprograms() {
    document.getElementById("myBtn").disabled = true;
    document.getElementById("myBtn").style.color = "black";
    document.getElementById("myBtn").style.backgroundColor = "grey";
    const ArrayofLine = document.getElementsByClassName('line');
    let inde = -1;

    Next.addEventListener('click', function() {
        inde++;
        ArrayofLine[inde].classList.add('line-highlight');
        if (inde > 0) ArrayofLine[inde - 1].classList.remove('line-highlight');
        if (inde == 0) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = "Here we declare the Header's file.";
        }
        if (inde == 1) {
            document.getElementById("Back").disabled = false;
            document.getElementById("Back").style.color = "";
            document.getElementById("Back").style.backgroundColor = "";
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = "Using namespacestd;";
        }
        if (inde == 2) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = "Here is A utility function that returns maximum of two integers.";
        }
        if (inde == 3) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = "Here we Returns the maximum value that can be put in a knapsack of capacity W.";
        }
        if (inde == 4) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = "Here is opening braces.";
        }
        if (inde == 5) {
            document.getElementById("Output2").innerHTML = "Here is Base Case .";
        }
        if (inde == 6) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = "Here we return zero.";
        }
        if (inde == 7) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = "Here is if statement";
        }
        if (inde == 8) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = "If weight of the nth item is more than Knapsack capacity W, then this item cannot be included  in the optimal solution .";
        }
        if (inde == 9) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = "Here is else statement.";
        }
        if (inde == 10) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = "Here we return the maximum of two cases: (1) nth item included (2) not included.";
        }
        if (inde == 11) {
            /*let elem = document.getElementById("codeContentBP22");
            elem.scrollIntoView(true);*/
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = "Here is closing braces.";
        }
        if (inde == 12) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = "Here is main function to drive the above code.";
        }
        if (inde == 13) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = "Here is opening braces.";
        }
        if (inde == 14) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = "Here is array val[] which hold value of knapsack.";
        }
        if (inde == 15) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = "Here is array wt[] which hold the weight of knapsack.";
        }
        if (inde == 16) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = "Here is variable W which holds some value.";
        }
        if (inde == 17) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = "Here is some operation is performed to find n.";
        }
        if (inde == 18) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = "Here we print the knapsack value via output statement.";
        }
        if (inde == 19) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = "Here is return statement which returns zero.";
        }

        if (inde == 20) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = " closing main function";
            document.getElementById("Next").disabled = true;
            document.getElementById("Next").style.color = "black";
            document.getElementById("Next").style.backgroundColor = "grey";
            document.getElementById("next-page").disabled = false;
            document.getElementById("next-page").style.color = "";
            document.getElementById("next-page").style.backgroundColor = "";
            alert("The code has ended now move to console window");
        }
    });
    Back.addEventListener('click', function() {
        inde--;
        ArrayofLine[inde].classList.add('line-highlight');
        if (inde >= 0) ArrayofLine[inde + 1].classList.remove('line-highlight');
        if (inde == 0) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = " Declaring Libraries of C++";
            document.getElementById("Back").disabled = true;
            document.getElementById("Back").style.color = "black";
            document.getElementById("Back").style.backgroundColor = "grey";
        }
        if (inde == 1) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = " Using namespace named std(Standard)";
        }
        if (inde == 2) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = "Here is A utility function that returns maximum of two integers.";
        }
        if (inde == 3) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = "Here we Returns the maximum value that can be put in a knapsack of capacity W.";
        }
        if (inde == 4) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = "Here is opening braces.";
        }
        if (inde == 5) {
            document.getElementById("Output2").innerHTML = "Here is Base Case .";
        }
        if (inde == 6) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = "Here we return zero.";
        }
        if (inde == 7) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = "Here is if statement";
        }
        if (inde == 8) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = "If weight of the nth item is more than Knapsack capacity W, then this item cannot be included  in the optimal solution .";
        }
        if (inde == 9) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = "Here is else statement.";
        }
        if (inde == 10) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = "Here we return the maximum of two cases: (1) nth item included (2) not included.";
        }
        if (inde == 11) {
            /*let elem = document.getElementById("codeContentBP22");
            elem.scrollIntoView(true);*/
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = "Here is closing braces.";
        }
        if (inde == 12) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = "Here is main function to drive the above code.";
        }
        if (inde == 13) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = "Here is opening braces.";
        }
        if (inde == 14) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = "Here is array val[] which hold value of knapsack.";
        }
        if (inde == 15) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = "Here is array wt[] which hold the weight of knapsack.";
        }
        if (inde == 16) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = "Here is variable W which holds some value.";
        }
        if (inde == 17) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = "Here is some operation is performed to find n.";
        }
        if (inde == 18) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = "Here we print the knapsack value via output statement.";
        }

        if (inde == 19) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = " Here we make a return statement which returns zero.";
            document.getElementById("Next").disabled = false;
            document.getElementById("Next").style.color = "";
            document.getElementById("Next").style.backgroundColor = "";
        }
        /*if (inde == 32) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = "returnfunction ran succesfully";
        }*/
        if (inde == 20) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = " closing main function";
            document.getElementById("Next").disabled = true;
            document.getElementById("Next").style.color = "black";
            document.getElementById("Next").style.backgroundColor = "grey";
        }
    });
}

function progs() {}

//var g = document.getElementById("textbox2").innerHTML = "Line 1"

function execute() {
    //for (i = 0; i < field1; i++) {
    count++;
    console.log(count);
    if (count <= 2) {
        var field1 = parseInt(document.getElementById("X").value);
        arr = Y.value.trim().split(' ');
        if (arr.length > field1)
            alert("Enter Correct values");
        else {
            arr.sort(function(a, b) { return a - b });
            document.getElementById("Output1").innerHTML = arr;
        }
        if (count == 2) {
            document.getElementById("myBtnexecute").disabled = true;
            document.getElementById("myBtnexecute").style.color = "black";
            document.getElementById("myBtnexecute").style.backgroundColor = "grey";
        }
    }
}
count = 0;
start = false;

document.getElementById("next-page").disabled = true;
document.getElementById("next-page").style.color = "black";
document.getElementById("next-page").style.backgroundColor = "grey";
document.getElementById("Back").disabled = true;
document.getElementById("Back").style.color = "black";
document.getElementById("Back").style.backgroundColor = "grey";

function Startprograms() {
    document.getElementById("myBtn").disabled = true;
    document.getElementById("myBtn").style.color = "black";
    document.getElementById("myBtn").style.backgroundColor = "grey";
    const ArrayofLine = document.getElementsByClassName('line');
    let inde = -1;

    Next.addEventListener('click', function() {
        inde++;
        ArrayofLine[inde].classList.add('line-highlight');
        if (inde > 0) ArrayofLine[inde - 1].classList.remove('line-highlight');
        if (inde == 0) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = "Here we declare the Header's file.";
        }
        if (inde == 1) {
            document.getElementById("Back").disabled = false;
            document.getElementById("Back").style.color = "";
            document.getElementById("Back").style.backgroundColor = "";
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = "Using namespacestd;";
        }
        if (inde == 2) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = "Here we make the count function which Returns the count of ways we can sum S[0...m-1] coins to get sum n.";
        }
        if (inde == 3) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = "Here is opening braces.";
        }
        if (inde == 4) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = "Here is if statement If n is 0 then there is 1 solution (do not include any coin).";
        }
        if (inde == 5) {
            document.getElementById("Output2").innerHTML = "Here we return 1.";
        }
        if (inde == 6) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = "Here is another if statement If n is less than 0 then no (solution exists).";
        }
        if (inde == 7) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = "Here we return 0.";
        }
        if (inde == 8) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = "Here is if statement If there are no coins and n is greater than 0, then no solution exist.";
        }
        if (inde == 9) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = "Here we return 0.";
        }
        if (inde == 10) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = "Here is count is sum of solutions (i)  including S[m-1] (ii) excluding S[m-1].";
        }
        if (inde == 11) {
            /*let elem = document.getElementById("codeContentBP22");
            elem.scrollIntoView(true);*/
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = "Here is closing braces.";
        }
        if (inde == 12) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = "Here is main function to driver above program.";
        }
        if (inde == 13) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = "Here is opening braces.";
        }
        if (inde == 14) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = "Here we have two variable integer type i and j.";
        }
        if (inde == 15) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = "Here is an array arr[].";
        }
        if (inde == 16) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = "Here is variable m which store some value.";
        }
        if (inde == 17) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = "Here we print the value via output statement.";
        }
        if (inde == 18) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = "Here is getchar().";
        }
        if (inde == 19) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = "Here is return statement which returns 0. ";
        }

        if (inde == 20) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = " closing main function";
            document.getElementById("next-page").disabled = false;
            document.getElementById("next-page").style.color = "";
            document.getElementById("next-page").style.backgroundColor = "";
            document.getElementById("Next").disabled = true;
            document.getElementById("Next").style.color = "black";
            document.getElementById("Next").style.backgroundColor = "grey";
            alert("The code has ended now move to console window");
        }
    });
    Back.addEventListener('click', function() {
        inde--;
        ArrayofLine[inde].classList.add('line-highlight');
        if (inde >= 0) ArrayofLine[inde + 1].classList.remove('line-highlight');
        if (inde == 0) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = " Declaring Libraries of C++";
            document.getElementById("Back").disabled = true;
            document.getElementById("Back").style.color = "black";
            document.getElementById("Back").style.backgroundColor = "grey";
        }
        if (inde == 1) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = " Using namespace named std(Standard)";
        }
        if (inde == 2) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = "Here we make the count function which Returns the count of ways we can sum S[0...m-1] coins to get sum n.";
        }
        if (inde == 3) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = "Here is opening braces.";
        }
        if (inde == 4) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = "Here is if statement If n is 0 then there is 1 solution (do not include any coin).";
        }
        if (inde == 5) {
            document.getElementById("Output2").innerHTML = "Here we return 1.";
        }
        if (inde == 6) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = "Here is another if statement If n is less than 0 then no (solution exists).";
        }
        if (inde == 7) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = "Here we return 0.";
        }
        if (inde == 8) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = "Here is if statement If there are no coins and n is greater than 0, then no solution exist.";
        }
        if (inde == 9) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = "Here we return 0.";
        }
        if (inde == 10) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = "Here is count is sum of solutions (i)  including S[m-1] (ii) excluding S[m-1].";
        }
        if (inde == 11) {
            /*let elem = document.getElementById("codeContentBP22");
            elem.scrollIntoView(true);*/
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = "Here is closing braces.";
        }
        if (inde == 12) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = "Here is main function to driver above program.";
        }
        if (inde == 13) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = "Here is opening braces.";
        }
        if (inde == 14) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = "Here we have two variable integer type i and j.";
        }
        if (inde == 15) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = "Here is an array arr[].";
        }
        if (inde == 16) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = "Here is variable m which store some value.";
        }
        if (inde == 17) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = "Here we print the value via output statement.";
        }
        if (inde == 18) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = "Here is getchar().";
        }

        if (inde == 19) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = " Here we make a return statement which returns zero.";
            document.getElementById("Next").disabled = false;
            document.getElementById("Next").style.color = "";
            document.getElementById("Next").style.backgroundColor = "";
        }
        /*if (inde == 32) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = "returnfunction ran succesfully";
        }*/
        if (inde == 20) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = " closing main function";
            document.getElementById("myBtnexecute").disabled = false;
            document.getElementById("myBtnexecute").style.color = "";
            document.getElementById("myBtnexecute").style.backgroundColor = "";
            document.getElementById("Next").disabled = true;
            document.getElementById("Next").style.color = "black";
            document.getElementById("Next").style.backgroundColor = "grey";
        }
    });
}

function progs() {}

//var g = document.getElementById("textbox2").innerHTML = "Line 1"

function execute() {
    //for (i = 0; i < field1; i++) {
    count++;
    console.log(count);
    if (count <= 2) {
        var field1 = parseInt(document.getElementById("X").value);
        arr = Y.value.trim().split(' ');
        if (arr.length > field1)
            alert("Enter Correct values");
        else {
            arr.sort(function(a, b) { return a - b });
            document.getElementById("Output1").innerHTML = arr;
        }
        if (count == 2) {
            document.getElementById("myBtnexecute").disabled = true;
            document.getElementById("myBtnexecute").style.color = "black";
            document.getElementById("myBtnexecute").style.backgroundColor = "grey";
        }
    }
}
count = 0;
start = false;

// document.getElementById("myBtnexecute").disabled = true;
// document.getElementById("myBtnexecute").style.color = "black";
// document.getElementById("myBtnexecute").style.backgroundColor = "grey";

document.getElementById("next-page").disabled = true;
document.getElementById("next-page").style.color = "black";
document.getElementById("next-page").style.backgroundColor = "grey";
document.getElementById("Back").disabled = true;
document.getElementById("Back").style.color = "black";
document.getElementById("Back").style.backgroundColor = "grey";

function Startprograms() {
    document.getElementById("myBtn").disabled = true;
    document.getElementById("myBtn").style.color = "black";
    document.getElementById("myBtn").style.backgroundColor = "grey";
    const ArrayofLine = document.getElementsByClassName('line');
    let inde = -1;

    Next.addEventListener('click', function() {
        inde++;
        ArrayofLine[inde].classList.add('line-highlight');
        if (inde > 0) ArrayofLine[inde - 1].classList.remove('line-highlight');
        if (inde == 0) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = "Here we include the required header file.";
        }
        if (inde == 1) {
            document.getElementById("Back").disabled = false;
            document.getElementById("Back").style.color = "";
            document.getElementById("Back").style.backgroundColor = "";
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = "Here we use using namespace std.";
        }
        if (inde == 2) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = "Here we are declaring a vector v to store our adjacency list.";
        }
        if (inde == 3) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = "Here we are declaring a vector visited to keep track of visited nodes.";
        }
        if (inde == 4) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = "Declaring function dfs";
        }
        if (inde == 5) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = "Main function";
        }
        if (inde == 6) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = "opening braces";
        }
        if (inde == 7) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = "declaring vertices, edges, x, y where x and y are two variables with the help of which we will form our adjacency list";
        }
        if (inde == 8) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = "taking number of vertices and edges as input ";
        }
        if (inde == 9) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = "for loop to take all the edges as input";
        }
        if (inde == 10) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = "opening braces";
        }
        if (inde == 11) {
            /*let elem = document.getElementById("codeContentBP22");
            elem.scrollIntoView(true);*/
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = "taking source and destination as input of edge where x is source and y is destination";
        }
        if (inde == 12) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = "decreasing x and y by one as we are using 0 based indexing";
        }
        if (inde == 13) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = "add y to x's list";
        }
        if (inde == 14) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = "add x to y's list";
        }
        if (inde == 15) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = "Here is closing braces.";
        }
        if (inde == 16) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = "calling dfs function on root vertex";
        }
        if (inde == 17) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = "Here is closing braces.";
        }
        if (inde == 18) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = "dfs function opening";
        }
        if (inde == 19) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = "Here is opening braces. ";
        }
        if (inde == 20) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = "here we take a variable len is which we store the count of all the vertices adjacent to our vertex on which dfs function is working now";
        }
        if (inde == 21) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = "marking our vertex as true in visited vector this means that we have visited this vertex";
        }
        if (inde == 22) {
            /*let elem = document.getElementById("codeContentBP22");
            elem.scrollIntoView(true);*/
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = "printing visited vertex";
        }
        if (inde == 23) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = "for loop till len";
        }
        if (inde == 24) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = "Here is opening braces.";
        }
        if (inde == 25) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = "here we are checking if we have visited this vertex or not";
        }
        if (inde == 26) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = "if we have visited that vertex then we continue and go to next adjacent vertex";
        }
        if (inde == 27) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = "else";
        }
        if (inde == 28) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = "we call dfs function on the vertex";
        }
        if (inde == 29) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = "Here is closing braces.";
        }
        if (inde == 30) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = "Here is closing braces.";
            // document.getElementById("myBtnexecute").disabled = false;
            // document.getElementById("myBtnexecute").style.color = "";
            // document.getElementById("myBtnexecute").style.backgroundColor = "";

            document.getElementById("next-page").disabled = false;
            document.getElementById("next-page").style.color = "";
            document.getElementById("next-page").style.backgroundColor = "";
            document.getElementById("Next").disabled = true;
            document.getElementById("Next").style.color = "black";
            document.getElementById("Next").style.backgroundColor = "grey";
            alert("The code has ended now move to console window");
        }
    });
    Back.addEventListener('click', function() {
        inde--;
        ArrayofLine[inde].classList.add('line-highlight');
        if (inde >= 0) ArrayofLine[inde + 1].classList.remove('line-highlight');
        if (inde == 0) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = " Here we include required header files.";
            document.getElementById("Back").disabled = true;
            document.getElementById("Back").style.color = "black";
            document.getElementById("Back").style.backgroundColor = "grey";
        }
        if (inde == 1) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = " Here we use using namespace std.";
        }
        if (inde == 2) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = "Here we are declaring a vector v to store our adjacency list.";
        }
        if (inde == 3) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = "Here we are declaring a vector visited to keep track of visited nodes.";
        }
        if (inde == 4) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = "Declaring function dfs";
        }
        if (inde == 5) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = "Main function";
        }
        if (inde == 6) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = "opening braces";
        }
        if (inde == 7) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = "declaring vertices, edges, x, y where x and y are two variables with the help of which we will form our adjacency list";
        }
        if (inde == 8) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = "taking number of vertices and edges as input ";
        }
        if (inde == 9) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = "for loop to take all the edges as input";
        }
        if (inde == 10) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = "opening braces";
        }
        if (inde == 11) {
            /*let elem = document.getElementById("codeContentBP22");
            elem.scrollIntoView(true);*/
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = "taking source and destination as input of edge where x is source and y is destination";
        }
        if (inde == 12) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = "decreasing x and y by one as we are using 0 based indexing";
        }
        if (inde == 13) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = "add y to x's list";
        }
        if (inde == 14) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = "add x to y's list";
        }
        if (inde == 15) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = "Here is closing braces.";
        }
        if (inde == 16) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = "calling dfs function on root vertex";
        }
        if (inde == 17) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = "Here is closing braces.";
        }
        if (inde == 18) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = "dfs function opening";
        }
        if (inde == 19) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = "Here is opening braces. ";
        }
        if (inde == 20) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = "here we take a variable len is which we store the count of all the vertices adjacent to our vertex on which dfs function is working now";
        }
        if (inde == 21) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = "marking our vertex as true in visited vector this means that we have visited this vertex";
        }
        if (inde == 22) {
            /*let elem = document.getElementById("codeContentBP22");
            elem.scrollIntoView(true);*/
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = "printing visited vertex";
        }
        if (inde == 23) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = "for loop till len";
        }
        if (inde == 24) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = "Here is opening braces.";
        }
        if (inde == 25) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = "here we are checking if we have visited this vertex or not";
        }
        if (inde == 26) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = "if we have visited that vertex then we continue and go to next adjacent vertex";
        }
        if (inde == 27) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = "else";
        }
        if (inde == 28) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = "we call dfs function on the vertex";
        }
        if (inde == 29) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = "Here is closing braces.";
            document.getElementById("Next").disabled = false;
            document.getElementById("Next").style.color = "";
            document.getElementById("Next").style.backgroundColor = "";
        }
        if (inde == 30) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = "Here is closing braces.";
            document.getElementById("Next").disabled = true;
            document.getElementById("Next").style.color = "black";
            document.getElementById("Next").style.backgroundColor = "grey";
        }

    });
}

function progs() {}

//var g = document.getElementById("textbox2").innerHTML = "Line 1"

function execute() {
    //for (i = 0; i < field1; i++) {
    count++;
    console.log(count);
    if (count <= 2) {
        var field1 = parseInt(document.getElementById("X").value);
        arr = Y.value.trim().split(' ');
        if (arr.length > field1)
            alert("Enter Correct values");
        else {
            arr.sort(function(a, b) { return a - b });
            document.getElementById("Output1").innerHTML = arr;
        }
        if (count == 2) {
            document.getElementById("myBtnexecute").disabled = true;
            document.getElementById("myBtnexecute").style.color = "black";
            document.getElementById("myBtnexecute").style.backgroundColor = "grey";
        }
    }
}
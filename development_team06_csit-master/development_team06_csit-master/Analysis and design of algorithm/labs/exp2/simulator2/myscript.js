count = 0;
start = false;

document.getElementById("myBtnexecute").disabled = true;
document.getElementById("myBtnexecute").style.color = "black";
document.getElementById("myBtnexecute").style.backgroundColor = "grey";
document.getElementById("Back").disabled = true;
document.getElementById("Back").style.color = "black";
document.getElementById("Back").style.backgroundColor = "grey";

function Startprograms() {
    document.getElementById("myBtn").disabled = true;
    document.getElementById("myBtn").style.color = "black";
    document.getElementById("myBtn").style.backgroundColor = "grey";
    const ArrayofLine = document.getElementsByClassName('line');
    let inde = -1;

    Next.addEventListener('click', function() {
        inde++;
        ArrayofLine[inde].classList.add('line-highlight');
        if (inde > 0) ArrayofLine[inde - 1].classList.remove('line-highlight');
        if (inde == 0) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = "Declaring Libraries of C++";
        }
        if (inde == 1) {
            document.getElementById("Back").disabled = false;
            document.getElementById("Back").style.color = "";
            document.getElementById("Back").style.backgroundColor = "";
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = "Declaring Libraries of C++";
        }
        if (inde == 2) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = "Declaring Libraries of C++";
        }
        if (inde == 3) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = "Using namespace named std(Standard)";
        }
        if (inde == 4) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = "Here is countSort fuction with return type void which takes an array as an argument";
        }
        if (inde == 5) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = "countSort function opening braces";
        }
        if (inde == 6) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = "Here we declare the max name variable which holds the value of maximum number between first and last element";
        }
        if (inde == 7) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = "Here we declare the min name variable which holds the value of minimum element between first and last";
        }
        if (inde == 8) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = "Here we declare the range name variable which holds the range";
        }
        if (inde == 9) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = "Here we call the count with range as an argument and output function with array size as an argument";
        }
        if (inde == 10) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = "Here we make the for loop which runs upto array size";
        }
        if (inde == 11) {
            /*let elem = document.getElementById("codeContentBP22");
            elem.scrollIntoView(true);*/
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = "";
        }
        if (inde == 12) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = "Again Here we declare the for loop which runs upto count size";
        }
        if (inde == 13) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = "";
        }
        if (inde == 14) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = "Again Here we make the for loop which runs until value of i becomes zero";
        }
        if (inde == 15) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = "For loop opening braces";
        }
        if (inde == 16) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = "";
        }
        if (inde == 17) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = "";
        }
        if (inde == 18) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = "For loop closing braces";
        }
        if (inde == 19) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = "Again here we make a for loop which runs upto array size";
        }
        if (inde == 20) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = "Here we store the elements of sorted array";
        }
        if (inde == 21) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = "countSort closing braces";
        }
        if (inde == 22) {
            /*let elem = document.getElementById("codeContentBP22");
            elem.scrollIntoView(true);*/
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = "Here we made the printArray fuction with return type void which is used to print the array";
        }
        if (inde == 23) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = "printArray function opening braces";
        }
        if (inde == 24) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = "Here we made the for loop which runs upto array size";
        }
        if (inde == 25) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = "Here we use the output statement to print the array";
        }
        if (inde == 26) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = "printArray function closing braces";
        }
        if (inde == 27) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = "Here we made the main function with return type int";
        }
        if (inde == 28) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = "main function opening braces";
        }
        if (inde == 29) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = "Here we initialise the size name variable ";
        }
        if (inde == 30) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = "Here we takes the size of array by input statement";
        }
        if (inde == 31) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = "Here we declare the array with size as an argument";
        }
        if (inde == 32) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = "Here we made the for loop which runs upto input size to store the elements in array";
        }
        if (inde == 33) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = "Here we store the elements in array through input statement ";
        }
        if (inde == 34) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = "Here we call the countSort function and give the array as an argument to sort the array";
        }
        if (inde == 35) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = "Here we call the printArray fuction and give the array as an argument to print the array";
        }
        
        if (inde == 36) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = "Here we return the value 0";
        }
        
        /*if (inde == 32) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = "returnfunction ran succesfully";
        }*/
        if (inde == 37) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = "closing main function";
            document.getElementById("myBtnexecute").disabled = false;
            document.getElementById("myBtnexecute").style.color = "";
            document.getElementById("myBtnexecute").style.backgroundColor = "";
            document.getElementById("Next").disabled = true;
            document.getElementById("Next").style.color = "black";
            document.getElementById("Next").style.backgroundColor = "grey";
            alert("The code has ended now move to console window");
        }
    });
    Back.addEventListener('click', function() {
        inde--;
        ArrayofLine[inde].classList.add('line-highlight');
        if (inde >= 0) ArrayofLine[inde + 1].classList.remove('line-highlight');
        if (inde == 0) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = " Declaring Libraries of C++";
            document.getElementById("Back").disabled = true;
            document.getElementById("Back").style.color = "black";
            document.getElementById("Back").style.backgroundColor = "grey";
        }
        if (inde == 1) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = "Declaring Libraries of C++";
        }
        if (inde == 2) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = "Declaring Libraries of C++";
        }
        if (inde == 3) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = "Using namespace named std(Standard)";
        }
        if (inde == 4) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = "Here is countSort fuction with return type void which takes an array as an argument";
        }
        if (inde == 5) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = "countSort function opening braces";
        }
        if (inde == 6) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = "Here we declare the max name variable which holds the value of maximum number between first and last element";
        }
        if (inde == 7) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = "Here we declare the min name variable which holds the value of minimum element between first and last";
        }
        if (inde == 8) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = "Here we declare the range name variable which holds the range";
        }
        if (inde == 9) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = "Here we call the count with range as an argument and output function with array size as an argument";
        }
        if (inde == 10) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = "Here we make the for loop which runs upto array size";
        }
        if (inde == 11) {
            /*let elem = document.getElementById("codeContentBP22");
            elem.scrollIntoView(true);*/
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = "";
        }
        if (inde == 12) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = "Again Here we declare the for loop which runs upto count size";
        }
        if (inde == 13) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = "";
        }
        if (inde == 14) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = "Again Here we make the for loop which runs until value of i becomes zero";
        }
        if (inde == 15) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = "For loop opening braces";
        }
        if (inde == 16) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = "";
        }
        if (inde == 17) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = "";
        }
        if (inde == 18) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = "For loop closing braces";
        }
        if (inde == 19) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = "Again here we make a for loop which runs upto array size";
        }
        if (inde == 20) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = "Here we store the elements of sorted array";
        }
        if (inde == 21) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = "countSort closing braces";
        }
        if (inde == 22) {
            /*let elem = document.getElementById("codeContentBP22");
            elem.scrollIntoView(true);*/
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = "Here we made the printArray fuction with return type void which is used to print the array";
        }
        if (inde == 23) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = "printArray function opening braces";
        }
        if (inde == 24) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = "Here we made the for loop which runs upto array size";
        }
        if (inde == 25) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = "Here we use the output statement to print the array";
        }
        if (inde == 26) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = "printArray function closing braces";
        }
        if (inde == 27) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = "Here we made the main function with return type int";
        }
        if (inde == 28) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = "main function opening braces";
        }
        if (inde == 29) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = "Here we initialise the size name variable ";
        }
        if (inde == 30) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = "Here we takes the size of array by input statement";
        }
        if (inde == 31) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = "Here we declare the array with size as an argument";
        }
        if (inde == 32) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = "Here we made the for loop which runs upto input size to store the elements in array";
        }
        if (inde == 33) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = "Here we store the elements in array through input statement ";
        }
        if (inde == 34) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = "Here we call the countSort function and give the array as an argument to sort the array";
        }
        if (inde == 35) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = "Here we call the printArray fuction and give the array as an argument to print the array";
        }       
        if (inde == 36) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = "Here we return the value 0";
            document.getElementById("Next").disabled = false;
            document.getElementById("Next").style.color = "";
            document.getElementById("Next").style.backgroundColor = "";
        }

        /*if (inde == 32) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = "returnfunction ran succesfully";
        }*/
        if (inde == 37) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = "closing main function";
            document.getElementById("myBtnexecute").disabled = false;
            document.getElementById("myBtnexecute").style.color = "";
            document.getElementById("myBtnexecute").style.backgroundColor = "";
            document.getElementById("Next").disabled = true;
            document.getElementById("Next").style.color = "black";
            document.getElementById("Next").style.backgroundColor = "grey";
        }
    });
}

function progs() {}

//var g = document.getElementById("textbox2").innerHTML = "Line 1"

function execute() {
    //for (i = 0; i < field1; i++) {
    count++;
    console.log(count);
    if (count <= 2) {
        var field1 = parseInt(document.getElementById("X").value);
        arr = Y.value.trim().split(' ');
        if (arr.length > field1)
            alert("Enter Correct values");
        else {
            arr.sort(function(a, b) { return a - b });
            document.getElementById("Output1").innerHTML = arr;
        }
        if (count == 2) {
            document.getElementById("myBtnexecute").disabled = true;
            document.getElementById("myBtnexecute").style.color = "black";
            document.getElementById("myBtnexecute").style.backgroundColor = "grey";
        }
    }
}
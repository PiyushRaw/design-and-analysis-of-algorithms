function getRandomInt(max) {
    return Math.floor(Math.random() * Math.floor(max));
}
var count = getRandomInt(3);
if (count == 0) {
    var cy = cytoscape({
        container: document.getElementById('cy'),

        boxSelectionEnabled: false,
        autounselectify: true,

        style: cytoscape.stylesheet()
            .selector('node')
            .style({
                'content': 'data(id)'
            })
            .selector('edge')
            .style({
                'curve-style': 'bezier',
                'target-arrow-shape': 'triangle',
                'width': 4,
                'line-color': '#ddd',
                'target-arrow-color': '#ddd'
            })
            .selector('.highlighted')
            .style({
                'background-color': '#61bffc',
                'line-color': '#61bffc',
                'target-arrow-color': '#61bffc',
                'transition-property': 'background-color, line-color, target-arrow-color',
                'transition-duration': '0.3s'
            }),

        elements: {
            nodes: [
                { data: { id: 'a' } },
                { data: { id: 'b' } },
                { data: { id: 'c' } },
                { data: { id: 'd' } },
                { data: { id: 'e' } }
            ],

            edges: [
                { data: { id: 'a"e', weight: 1, source: 'a', target: 'e' } },
                { data: { id: 'ab', weight: 3, source: 'a', target: 'b' } },
                { data: { id: 'be', weight: 4, source: 'b', target: 'e' } },
                { data: { id: 'bc', weight: 5, source: 'b', target: 'c' } },
                { data: { id: 'ce', weight: 6, source: 'c', target: 'e' } },
                { data: { id: 'cd', weight: 2, source: 'c', target: 'd' } },
                { data: { id: 'de', weight: 7, source: 'd', target: 'e' } }
            ]
        },

        layout: {
            name: 'breadthfirst',
            directed: true,
            roots: '#a',
            padding: 10
        }
    });
}
if (count == 1) {
    var cy1 = cytoscape({
        container: document.getElementById('cy'),

        boxSelectionEnabled: false,
        autounselectify: true,

        style: cytoscape.stylesheet()
            .selector('node')
            .style({
                'content': 'data(id)'
            })
            .selector('edge')
            .style({
                'curve-style': 'bezier',
                'target-arrow-shape': 'triangle',
                'width': 4,
                'line-color': '#ddd',
                'target-arrow-color': '#ddd'
            })
            .selector('.highlighted')
            .style({
                'background-color': '#61bffc',
                'line-color': '#61bffc',
                'target-arrow-color': '#61bffc',
                'transition-property': 'background-color, line-color, target-arrow-color',
                'transition-duration': '0.3s'
            }),

        elements: {
            nodes: [
                { data: { id: 'a' } },
                { data: { id: 'b' } },
                { data: { id: 'c' } },
                { data: { id: 'd' } },
                { data: { id: 'e' } },
                { data: { id: 'f' } }
            ],

            edges: [
                { data: { id: 'a"e', weight: 1, source: 'a', target: 'e' } },
                { data: { id: 'ac', weight: 3, source: 'a', target: 'c' } },

                { data: { id: 'ad', weight: 5, source: 'a', target: 'd' } },
                { data: { id: 'cb', weight: 6, source: 'c', target: 'b' } },
                { data: { id: 'df', weight: 2, source: 'd', target: 'f' } }

            ]
        },

        layout: {
            name: 'breadthfirst',
            directed: true,
            roots: '#a',
            padding: 10
        }
    });
}
if (count == 2) {
    var cy2 = cytoscape({
        container: document.getElementById('cy'),

        boxSelectionEnabled: false,
        autounselectify: true,

        style: cytoscape.stylesheet()
            .selector('node')
            .style({
                'content': 'data(id)'
            })
            .selector('edge')
            .style({
                'curve-style': 'bezier',
                'target-arrow-shape': 'triangle',
                'width': 4,
                'line-color': '#ddd',
                'target-arrow-color': '#ddd'
            })
            .selector('.highlighted')
            .style({
                'background-color': '#61bffc',
                'line-color': '#61bffc',
                'target-arrow-color': '#61bffc',
                'transition-property': 'background-color, line-color, target-arrow-color',
                'transition-duration': '0.3s'
            }),

        elements: {
            nodes: [
                { data: { id: 'a' } },
                { data: { id: 'b' } },
                { data: { id: 'c' } },
                { data: { id: 'd' } },
                { data: { id: 'e' } }
            ],

            edges: [
                { data: { id: 'a"e', weight: 1, source: 'a', target: 'e' } },
                { data: { id: 'eb', weight: 3, source: 'a', target: 'b' } },
                { data: { id: 'ad', weight: 5, source: 'e', target: 'd' } },
                { data: { id: 'ac', weight: 6, source: 'e', target: 'c' } }
            ]
        },

        layout: {
            name: 'breadthfirst',
            directed: true,
            roots: '#a',
            padding: 10
        }
    });
}
f = [];
f.push(cy);
f.push(cy1);
f.push(cy2);
var rc = f[count];
var dfs = rc.elements().dfs('#a', function() {}, true);

var i = 0;
var highlightNextEle = function() {
    if (i < dfs.path.length) {
        dfs.path[i].addClass('highlighted');
        i++;
        setTimeout(highlightNextEle, 900);
    }
    if (i == dfs.path.length) {
        let pr = document.querySelector('#tex');
        if (count == 0)
            pr.innerHTML = "Traversal : a b c d e";
        if (count == 1)
            pr.innerHTML = "Traversal : a d f c b e";
        if (count == 2)
            pr.innerHTML = "Traversal : a b e c d";
    }
};

// kick off first highlight
highlightNextEle();
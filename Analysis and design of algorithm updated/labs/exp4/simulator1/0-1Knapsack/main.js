function execute() {
    //count++;
    var elem = document.getElementById("exec");
    if (elem.innerHTML == "Calculate") {
        var w = [null],
            v = [null],
            capacity,
            totalNumberOfItems = 3;
        var x = parseInt(document.getElementById("item1w").value);
        w.push(x);
        x = parseInt(document.getElementById("item2w").value);
        w.push(x);
        x = parseInt(document.getElementById("item3w").value);
        w.push(x);
        x = parseInt(document.getElementById("item1v").value);
        v.push(x);

        x = parseInt(document.getElementById("item2v").value);
        v.push(x);

        x = parseInt(document.getElementById("item3v").value);
        v.push(x);

        capacity = parseInt(document.getElementById("capacity").value);


        // var maximizeTotal = knapsack(totalNumberOfItems,
        //     capacity,
        //     w,
        //     v);

        // console.log('Recursion Maximum is : ', maximizeTotal); //returns 18

        /*
         *   USING DYNAMIC PROGRAMMING:
         *       in this approach we are doing to store the number of items (n) and the 
         *        capacity in a two dimensional array 
         *        dp = [ [a, b], [c, d], [e, f] ]
         *        therefore dp[0][0] => a; or dp[0][1] => b;
         */

        var dp = [
            [undefined, undefined],
            [undefined, undefined],
            [undefined, undefined],
            [undefined, undefined],
            [undefined, undefined],
            [undefined, undefined]
        ];

        function knapsackDP(N, capacity, w, v) {

            var finalResult;

            //check to see if the result is already stored in the array. if it is return that instead
            if (dp[N][capacity] !== undefined) {
                return dp[N][capacity];
            }

            //define basecase. if capacity or number of items is zero, then final result is zero
            if (N === 0 || capacity === 0) {
                finalResult = 0;
            } else if (w[N] > capacity) {
                finalResult = knapsackDP(N - 1, capacity, w, v);
            } else {
                var donttake = knapsackDP(N - 1, capacity, w, v);
                var takeit = v[N] + knapsackDP(N - 1, capacity - w[N], w, v);
                finalResult = Math.max(donttake, takeit);
            }

            //save the result in the array
            dp[N][capacity] = finalResult;

            //return the final result
            return finalResult;
        }

        var total_profit = knapsackDP(totalNumberOfItems,
            capacity,
            w,
            v);

        var weigh = [];
        var ki = 1;

        while (ki--) {
            if (v[1] + v[2] == total_profit) {
                weigh[0] = w[1];
                weigh[1] = w[2];
                weigh[2] = null;
                var remain = capacity - weigh[0] - weigh[1];
                weigh[3] = remain;
                console.log("first");

                break;
            }
            if (v[3] + v[2] == total_profit) {
                weigh[0] = null
                    //weigh[1] = null;
                weigh[1] = w[2];
                weigh[2] = w[3];
                var remain = capacity - weigh[1] - weigh[2];
                weigh[3] = remain;
                //console.log("sec");
                break;
            }
            if (v[1] + v[3] == total_profit) {
                weigh[0] = w[1];
                weigh[1] = null;
                weigh[2] = w[3];

                var remain = capacity - weigh[2] - weigh[3];
                weigh[3] = remain;
                //console.log("third");
                break;
            }
            if (v[1] + v[2] + v[3] == total_profit) {
                weigh[0] = w[1];
                weigh[1] = w[2];
                weigh[2] = w[3];
                var remain = capacity - weigh[0] - weigh[1] - weigh[2];
                weigh[3] = remain;
            }
        }
        //console.log(weigh);

        console.log('Dynamic Programming Maximum is : ', total_profit); //returns 18
        let pro = document.querySelector('.profit');
        pro.innerText = `Maximum Profit: ${total_profit}`;

        //console.log(capacity);
        var ctx = document.getElementById('myChart').getContext('2d');

        var chart = new Chart(ctx, {
            // The type of chart we want to create
            type: 'doughnut',

            // The data for our dataset
            data: {
                labels: ["Item 1", "Item 2", "Item 3", "Unused"],
                datasets: [{
                    label: "My First dataset",
                    backgroundColor: ['#FF0000', '#5200FF', '#008000', '#455A64'],
                    borderColor: '#fff',
                    data: weigh,
                }]
            },

            // Configuration options go here
            options: {}
        });
        elem.innerHTML = "Clear";
    } else {
        window.location.reload();

    }
}
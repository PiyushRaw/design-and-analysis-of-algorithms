function execute() {
    var w = [];
    var x = parseInt(document.getElementById("item1w").value);
    w.push(x);
    x = parseInt(document.getElementById("item2w").value);
    w.push(x);
    x = parseInt(document.getElementById("item3w").value);
    w.push(x);

    var capacity = parseInt(document.getElementById("capacity").value);
    var makeChange = function(total) {
        var count = 0;
        var changer = function(index, value) {

            var currentCoin = w[index];

            if (index === 0) {
                if (value % currentCoin === 0) {
                    count++;
                }
                return;
            }

            while (value >= 0) {
                changer(index - 1, value);
                value -= currentCoin;
            }
        }
        changer(w.length - 1, total);
        return count;
    };
    var final_result = makeChange(capacity);
    let pro = document.querySelector('.profit');
    pro.innerText = `Total Ways to make the change: ${final_result}`;

}
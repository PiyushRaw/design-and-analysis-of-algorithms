function ind(dic_name, dic_name2, value_find) {
    for (i = 0; i < dic_name.length; i++) {
        if (dic_name2[i] == value_find.price && dic_name[i] == value_find.weight) {
            return i;
        }
    }
    return undefined;
}

function execute() {
    var elem = document.getElementById("exec");
    if (elem.innerHTML == "Calculate") {
        var w = [];
        var v = [];
        var ratio = [];
        //var inde = [];

        var x = parseInt(document.getElementById("item1w").value);
        w.push(x);
        x = parseInt(document.getElementById("item2w").value);
        w.push(x);
        x = parseInt(document.getElementById("item3w").value);
        w.push(x);

        x = parseInt(document.getElementById("item1v").value);
        v.push(x);
        //ratio.push(v[0] / w[0]);
        x = parseInt(document.getElementById("item2v").value);
        v.push(x);
        //ratio.push(v[1] / w[1]);
        x = parseInt(document.getElementById("item3v").value);
        v.push(x);
        //ratio.push(v[2] / w[2]);
        var capacity = parseInt(document.getElementById("capacity").value);
        //console.log(capacity);
        var ratio1 = [
            { price: v[0], weight: w[0] },
            { price: v[1], weight: w[1] },
            { price: v[2], weight: w[2] },
        ]

        ratio1.sort((a, b) => {
            var r2 = (b.price / b.weight);
            var r1 = (a.price / a.weight);
            //console.log(r1, ' ', r2);

            return r2 - r1;
        });
        weigh = [];
        var final_result = 0,
            curr_weight = 0,
            sum = 0;
        for (var va in ratio1) {
            var key = ind(w, v, ratio1[va]);

            //console.log(ratio1[va].weight, ' ', ratio1[va].price);

            if (curr_weight + ratio1[va].weight <= capacity) {
                curr_weight += ratio1[va].weight;
                final_result += ratio1[va].price;
                weigh[key] = w[key];
                sum = sum + w[key];
            } else {
                var remain = capacity - curr_weight;
                final_result += ratio1[va].price * (remain / ratio1[va].weight);
                weigh[key] = remain / ratio1[va].weight;
                sum = sum + remain;
                break;
            }



        }
        weigh[3] = capacity - sum;
        // var sum = 0;
        // for (var f in weigh) {
        //     sum = sum + weigh[f];
        // }
        // if (sum < capacity) weigh.push(capacity - sum);
        let pro = document.querySelector('.profit');
        pro.innerText = `Maximum Profit: ${final_result}`;
        var ctx = document.getElementById('myChart').getContext('2d');

        var chart = new Chart(ctx, {
            // The type of chart we want to create
            type: 'doughnut',

            // The data for our dataset
            data: {
                labels: ["Item 1", "Item 2", "Item 3", "Unused"],
                datasets: [{
                    label: "My First dataset",
                    backgroundColor: ['#FF0000', '#5200FF', '#008000', '#455A64'],
                    borderColor: '#fff',
                    data: weigh,
                }]
            },

            // Configuration options go here
            options: {}
        });
        elem.innerHTML = "Clear";
    } else {
        window.location.reload();
    }
}
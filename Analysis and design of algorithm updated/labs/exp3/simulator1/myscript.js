count = 0;
start = false;


document.getElementById("Back").disabled = true;
document.getElementById("Back").style.color = "black";
document.getElementById("Back").style.backgroundColor = "grey";
document.getElementById("next-page").disabled = true;
document.getElementById("next-page").style.color = "black";
document.getElementById("next-page").style.backgroundColor = "grey";

function Startprograms() {
    document.getElementById("myBtn").disabled = true;
    document.getElementById("myBtn").style.color = "black";
    document.getElementById("myBtn").style.backgroundColor = "grey";
    const ArrayofLine = document.getElementsByClassName('line');
    let inde = -1;

    Next.addEventListener('click', function() {
        inde++;
        ArrayofLine[inde].classList.add('line-highlight');
        if (inde > 0) ArrayofLine[inde - 1].classList.remove('line-highlight');
        if (inde == 0) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = "Here we declare the Header's file.";
        }
        if (inde == 1) {
            document.getElementById("Back").disabled = false;
            document.getElementById("Back").style.color = "";
            document.getElementById("Back").style.backgroundColor = "";
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = "Here we declare the Header's file.";
        }
        if (inde == 2) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = "Number of vertices in the graph.";
        }
        if (inde == 3) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = "A utility function to find the vertex with minimum distance value, from the set of vertices not yet included in shortest path tree.";
        }
        if (inde == 4) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = "Here is the open braces.";
        }
        if (inde == 5) {
            document.getElementById("Output2").innerHTML = "Here we Initialize min value.";
        }
        if (inde == 6) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = "Here we make the for loop which runs upto V.";
        }
        if (inde == 7) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = "Here is if statement.";
        }
        if (inde == 8) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = "Here we hold the minimum value and minimum index.";
        }
        if (inde == 9) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = "Here we return the minimum index.";
        }
        if (inde == 10) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = "Here is the closing braces.";
        }
        if (inde == 11) {
            /*let elem = document.getElementById("codeContentBP22");
            elem.scrollIntoView(true);*/
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = "A utility function to print the constructed distance array.";
        }
        if (inde == 12) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = "Here is the open braces.";
        }
        if (inde == 13) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = "Here we print something via output statement.";
        }
        if (inde == 14) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = "Here we declare the for loop which runs upto V.";
            
        }
        if (inde == 15) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = "Here we print the value of i and dist[i] via output statement.";
        }
        if (inde == 16) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = "Here is closing braces.";
        }
        if (inde == 17) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = "Function that implements Dijkstra's single source shortest path algorithm for a graph represented using adjacency matrix representation.";
        }
        if (inde == 18) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = "Here is opening braces.";
        }
        if (inde == 19) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = "The output array.  dist[i] will hold the shortest distance from src to i. ";
        }
        if (inde == 20) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = "sptSet[i] will be true if vertex i is included in shortest  path tree or shortest distance from src to i is finalized. ";
        }
        if (inde == 21) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = "Here we make the for loop which runs upto V.";
            scrolldown();
        }
        if (inde == 22) {
            /*let elem = document.getElementById("codeContentBP22");
            elem.scrollIntoView(true);*/
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = "Initialize all distances as INFINITE and stpSet[] as false. ";
        }
        if (inde == 23) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = "Distance of source vertex from itself is always 0.";
        }
        if (inde == 24) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = "Here we make the for loop which runs upto V to find the shortest path for all vertices";
        }
        if (inde == 25) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = "Here is opening braces.";
        }
        if (inde == 26) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = "Pick the minimum distance vertex from the set of vertices not yet processed. u is always equal to src in the first iteration.";
        }
        if (inde == 27) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = "Mark the picked vertex as processed.";
        }
        if (inde == 28) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = "Here we make the for loop which runs upto V to Update dist value of the adjacent vertices of the picked vertex.";
        }
        if (inde == 30) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = "Update dist[v] only if is not in sptSet, there is an edge from  u to v, and total weight of path from src to  v through u is  smaller than current value of dist[v].";
        }
        if (inde == 29) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = "Here we hold the some value in dist[v].";
        }
        if (inde == 31) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = "Here is the closing braces.";
        }
        if (inde == 32) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = "print the constructed distance array.";
        }
        if (inde == 33) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = "Here is closing braces.";
        }
        if (inde == 34) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = "Here we make the main function to drive the above program.";
            scrolldownwards();
        }
        if (inde == 35) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = "Here is the opening braces.";
        }
        if (inde == 36) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = "Here we make the graph by giving it predifined values in it.";
        }
        if (inde == 37) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = "Here we call the dijkasta function with graph as a argument.";
        }
        if (inde == 38) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = "Here is return statement which retunrn 0.";
        }

        if (inde == 39) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = " closing main function";
            document.getElementById("next-page").disabled = false;
            document.getElementById("next-page").style.color = "";
            document.getElementById("next-page").style.backgroundColor = "";
            document.getElementById("Next").disabled = true;
            document.getElementById("Next").style.color = "black";
            document.getElementById("Next").style.backgroundColor = "grey";
            alert("The code has ended now move to console window");
        }
    });
    Back.addEventListener('click', function() {
        inde--;
        ArrayofLine[inde].classList.add('line-highlight');
        if (inde >= 0) ArrayofLine[inde + 1].classList.remove('line-highlight');
        if (inde == 0) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = " Declaring Libraries of C++";
            document.getElementById("Back").disabled = true;
            document.getElementById("Back").style.color = "black";
            document.getElementById("Back").style.backgroundColor = "grey";
        }
        if (inde == 1) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = " Using namespace named std(Standard)";
        }
        if (inde == 2) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = "Number of vertices in the graph.";
        }
        if (inde == 3) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = "A utility function to find the vertex with minimum distance value, from the set of vertices not yet included in shortest path tree.";
        }
        if (inde == 4) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = "Here is the open braces.";
        }
        if (inde == 5) {
            document.getElementById("Output2").innerHTML = "Here we Initialize min value.";
        }
        if (inde == 6) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = "Here we make the for loop which runs upto V.";
        }
        if (inde == 7) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = "Here is if statement.";
        }
        if (inde == 8) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = "Here we hold the minimum value and minimum index.";
        }
        if (inde == 9) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = "Here we return the minimum index.";
        }
        if (inde == 10) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = "Here is the closing braces.";
        }
        if (inde == 11) {
            /*let elem = document.getElementById("codeContentBP22");
            elem.scrollIntoView(true);*/
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = "A utility function to print the constructed distance array.";
        }
        if (inde == 12) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = "Here is the open braces.";
        }
        if (inde == 13) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = "Here we print something via output statement.";
        }
        if (inde == 14) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = "Here we declare the for loop which runs upto V.";
            scrollup();
        }
        if (inde == 15) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = "Here we print the value of i and dist[i] via output statement.";
        }
        if (inde == 16) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = "Here is closing braces.";
        }
        if (inde == 17) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = "Function that implements Dijkstra's single source shortest path algorithm for a graph represented using adjacency matrix representation.";
        }
        if (inde == 18) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = "Here is opening braces.";
        }
        if (inde == 19) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = "The output array.  dist[i] will hold the shortest distance from src to i. ";
        }
        if (inde == 20) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = "sptSet[i] will be true if vertex i is included in shortest  path tree or shortest distance from src to i is finalized. ";
        }
        if (inde == 21) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = "Here we make the for loop which runs upto V.";

            scrollup();
        }
        if (inde == 22) {
            /*let elem = document.getElementById("codeContentBP22");
            elem.scrollIntoView(true);*/
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = "Initialize all distances as INFINITE and stpSet[] as false. ";
        }
        if (inde == 23) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = "Distance of source vertex from itself is always 0.";
        }
        if (inde == 24) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = "Here we make the for loop which runs upto V to find the shortest path for all vertices";
        }
        if (inde == 25) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = "Here is opening braces.";
        }
        if (inde == 26) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = "Pick the minimum distance vertex from the set of vertices not yet processed. u is always equal to src in the first iteration.";
        }
        if (inde == 27) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = "Mark the picked vertex as processed.";
        }
        if (inde == 28) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = "Here we make the for loop which runs upto V to Update dist value of the adjacent vertices of the picked vertex.";
        }
        if (inde == 30) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = "Update dist[v] only if is not in sptSet, there is an edge from  u to v, and total weight of path from src to  v through u is  smaller than current value of dist[v].";
        }
        if (inde == 29) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = "Here we hold the some value in dist[v].";
        }
        if (inde == 31) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = "Here is the closing braces.";
        }
        if (inde == 32) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = "print the constructed distance array.";
        }
        if (inde == 33) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = "Here is closing braces.";
        }
        if (inde == 34) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = "Here we make the main function to drive the above program.";
        }
        if (inde == 35) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = "Here is the opening braces.";
        }
        if (inde == 36) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = "Here we make the graph by giving it predifined values in it.";
        }
        if (inde == 37) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = "Here we call the dijkasta function with graph as a argument.";
        }
        if (inde == 38) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = " Here we make a return statement which returns zero.";
            document.getElementById("Next").disabled = false;
            document.getElementById("Next").style.color = "";
            document.getElementById("Next").style.backgroundColor = "";
        }
        /*if (inde == 32) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = "returnfunction ran succesfully";
        }*/
        if (inde == 49) {
            document.getElementById("Output2").innerHTML = "";
            document.getElementById("Output2").innerHTML = " closing main function";

            document.getElementById("Next").disabled = true;
            document.getElementById("Next").style.color = "black";
            document.getElementById("Next").style.backgroundColor = "grey";
        }
    });
}

function progs() {}

//var g = document.getElementById("textbox2").innerHTML = "Line 1"

function execute() {
    //for (i = 0; i < field1; i++) {
    count++;
    console.log(count);
    if (count <= 2) {
        var field1 = parseInt(document.getElementById("X").value);
        arr = Y.value.trim().split(' ');
        if (arr.length > field1)
            alert("Enter Correct values");
        else {
            arr.sort(function(a, b) { return a - b });
            document.getElementById("Output1").innerHTML = arr;
        }
        if (count == 2) {
            document.getElementById("myBtnexecute").disabled = true;
            document.getElementById("myBtnexecute").style.color = "black";
            document.getElementById("myBtnexecute").style.backgroundColor = "grey";
        }
    }
}


function scrolldown() {

    document.getElementById("basicPointerText").scrollTo(0,800);
}


function scrollup() {

    document.getElementById("basicPointerText").scrollTo(0,-800);
}




